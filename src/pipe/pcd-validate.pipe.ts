import { Injectable, ArgumentMetadata, BadRequestException, ValidationPipe, UnprocessableEntityException } from '@nestjs/common';

@Injectable()
export class IctValidateInputPipe extends ValidationPipe {
   public async transform(value, metadata: ArgumentMetadata) {
      try {
        return await super.transform(value, metadata);
      } catch (e) {
         if (e instanceof BadRequestException) {
            // throw new UnprocessableEntityException(this.handleError(e.message));  //commented after migration
            let err: Object = e.getResponse();
            console.log("====err1====",err["message"]);
            throw new UnprocessableEntityException(err["message"]);
         }
      }
   }

   private handleError(errors) {
        return errors.map(error => error.constraints);
   }
}