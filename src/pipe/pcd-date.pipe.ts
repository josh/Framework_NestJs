export function convertDate(date: Date, format?: string): Date {
    if (!date || date.toString() == "" || date.toString() == "null")
        return null;

    date = new Date(date);
    if (!format)
        format = "yyyy-MM-dd";
    let day = ('0' + date.getDate()).slice(-2);
    let month = ('0' + (date.getMonth() + 1)).slice(-2);
    let year = date.getFullYear();
    let hours = ('0' + date.getHours() || '00').slice(-2);
    let minutes = ('0' + date.getMinutes() || '00').slice(-2);
    let seconds = ('0' + date.getSeconds() || '00').slice(-2);
    let stringDt = format;
    if (format.includes("yyyy")) stringDt = stringDt.replace("yyyy", year.toString());
    if (format.includes("MM")) stringDt = stringDt.replace("MM", month.toString());
    if (format.includes("dd")) stringDt = stringDt.replace("dd", day.toString());
    if (format.includes("hh")) stringDt = stringDt.replace("hh", hours.toString());
    if (format.includes("mm")) stringDt = stringDt.replace("mm", minutes.toString());
    if (format.includes("ss")) stringDt = stringDt.replace("ss", seconds.toString());

    let convDate = new Date(stringDt);

    return convDate;
}

export function convertDateString(date: Date, format?: string): string {
    if (!date || date.toString() == "null")
        return null;

    date = new Date(date);

    if (!format)
        format = "yyyy-MM-dd";
    let day = ('0' + date.getDate()).slice(-2);
    let month = ('0' + (date.getMonth() + 1)).slice(-2);
    let year = date.getFullYear();
    let hours = ('0' + date.getHours() || '00').slice(-2);
    let minutes = ('0' + date.getMinutes() || '00').slice(-2);
    let seconds = ('0' + date.getSeconds() || '00').slice(-2);

    let stringDt = format;
    if (format.includes("yyyy")) stringDt = stringDt.replace("yyyy", year.toString());
    if (format.includes("MM")) stringDt = stringDt.replace("MM", month.toString());
    if (format.includes("dd")) stringDt = stringDt.replace("dd", day.toString());
    if (format.includes("hh")) stringDt = stringDt.replace("hh", hours.toString());
    if (format.includes("mm")) stringDt = stringDt.replace("mm", minutes.toString());
    if (format.includes("ss")) stringDt = stringDt.replace("ss", seconds.toString());
    if (format.includes("MON")) stringDt = stringDt.replace("MON", shortMonth[parseInt(month.toString()) - 1]);
    if (format.includes("MONTH")) stringDt = stringDt.replace("MONTH", month[parseInt(month.toString()) - 1]);


    return stringDt;
}

const month = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"];

const shortMonth = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];