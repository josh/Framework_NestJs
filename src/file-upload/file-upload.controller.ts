import { BadRequestException, Controller, Delete, Get, NotFoundException, Param, Post, Req, Res, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { existsSync, mkdirSync } from 'fs';
import { diskStorage } from 'multer';

import { GetUser } from 'src/auth/dto/get-user.decorator';
import { FileUploadService } from './file-upload.service';
import * as fs from 'fs';
import * as path from 'path';
import { join, extname } from 'path';
import { TenderDocumentDto } from 'src/dto/tender-document.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { log } from 'console';

export function createFolders(filePath: string) {

    const splitPath = filePath.split('/');
    splitPath.reduce((path, subPath) => {
        let currentPath;
        if (subPath != '.') {
            currentPath = path + '/' + subPath;

            if (!existsSync(currentPath)) {
                mkdirSync(currentPath);
            }
        }
        else {
            currentPath = subPath;
        }
        return currentPath

    }, '')
}


@Controller('file-upload')
export class FileUploadController {
    constructor(
        private iservice: FileUploadService,
    ) {
    }

   
    @Post('user-documents/:id/:userId')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: (req, res, cb) => {
                const parentFolder: string = './uploads/userDocuments/' + req.params.id;
                createFolders(parentFolder);
                const childFolder: string = parentFolder + '/' + req.params.userId;
                createFolders(childFolder);
                return cb(null, childFolder);
            },
            filename: (req, file, cb) => {
                // Use the original filename instead of generating a random name
                return cb(null, `${file.originalname}`);
            }
        })
    }))
    userDoc(@UploadedFile() file) {
        if (!file) {
            return { error: 'No file uploaded.' };
        }
        // To return the size of the document in kb
        const fileSizeKB = Math.ceil(file.size / 1024);
        return { fileName: file.filename, filePath: file.path, fileSizeKB };
    }



    @UseGuards(AuthGuard())
    @Post('Bank-documents/:id')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            // destination: './uploads/cars',
            destination: (req, res, cb) => {
              

                const filePath: string = './uploads/BankDocuments/' + req.params.id;
                createFolders(filePath);
                return cb(null, filePath);
            },
            filename: (req, file, cb) => {
                // Use the original filename instead of generating a random name
                return cb(null, `${file.originalname}`);
            }
        })
    }))
    bankDoc(@UploadedFile() file) {
        if (!file) {
            return { error: 'No file uploaded.' };
        }
        //To return the size of the document in kb
        const fileSizeKB = Math.ceil(file.size / 1024);
        return { fileName: file.filename, filePath: file.path, fileSizeKB };
    }

    @UseGuards(AuthGuard())
    @Post('drawings/:id')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            // destination: './uploads/cars',
            destination: (req, res, cb) => {
                // console.log(req);

                const filePath: string = './uploads/Drawings/' + req.params.id;
                createFolders(filePath);
                return cb(null, filePath);
            },
            filename: (req, file, cb) => {
                // Use the original filename instead of generating a random name
                return cb(null, `${file.originalname}`);
            }
        })
    }))
    drawings(@UploadedFile() file) {
        if (!file) {
            return { error: 'No file uploaded.' };
        }
        //To return the size of the document in kb
        const fileSizeKB = Math.ceil(file.size / 1024);
        return { fileName: file.filename, filePath: file.path, fileSizeKB };
    }

   


    @UseGuards(AuthGuard())
    @Post('notice/:id')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            // destination: './uploads/cars',
            destination: (req, res, cb) => {
                // console.log(req);

                const filePath: string = './uploads/Notice/' + req.params.id;
                createFolders(filePath);
                return cb(null, filePath);
            },
            filename: (req, file, cb) => {
                // Use the original filename instead of generating a random name
                return cb(null, `${file.originalname}`);
            }
        })
    }))

    notice(@UploadedFile() file) {
        if (!file) {
            return { error: 'No file uploaded.' };
        }
        //To return the size of the document in kb
        const fileSizeKB = Math.ceil(file.size / 1024);
        return { fileName: file.filename, filePath: file.path, fileSizeKB };
    }


    @Get('notice/:id/:imgpath')
    getNotice(
        @Param('imgpath') image: string,
        @Param('id') id: string,
        @Res() res,
        @Req() req,
    ) {
        return res.sendFile(image, { root: './uploads/Notice/' + id }, function (err) {
            if (err) {
                console.log(err.status);
                res.status(err.status).end();
            }
        })
    }




    @UseGuards(AuthGuard())
    @Get('Bank-Documents/:id/:imgpath')
    getBankDocuments(
        @Param('imgpath') image: string,
        @Param('id') id: string,
        @Res() res,
        @Req() req,

    ) {
        return res.sendFile(image, { root: './uploads/BankDocuments/' + id }, function (err) {
            if (err) {
                console.log(err.status);
                res.status(err.status).end();
            }
        })
    }

   
    @Get('user-Documents/:id/:userId/:imgpath')
    getUserDocuments(
        @Param('imgpath') image: string,
        @Param('userId') userId: string,
        @Param('id') id: string,
        @Res() res,
        @Req() req,

    ) {
        return res.sendFile(image, { root: './uploads/userDocuments/' + id + '/' + userId }, function (err) {
            if (err) {
                console.log(err.status);
                res.status(err.status).end();
            }
        })
    }


  
  

}