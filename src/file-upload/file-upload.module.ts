import { Module } from '@nestjs/common';
import { FileUploadController } from './file-upload.controller';
import { FileUploadService } from './file-upload.service';
import { MulterModule } from '@nestjs/platform-express';
import { AuthModule } from 'src/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
 
@Module({
  imports: [
    MulterModule.register({
      dest: './uploads'
    }),
     AuthModule,
  ],
  controllers: [FileUploadController],
  providers: [FileUploadService]
})
export class FileUploadModule { }
