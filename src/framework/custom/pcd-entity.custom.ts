import { CreateDateColumn, Column, UpdateDateColumn, VersionColumn, BaseEntity, Entity } from "typeorm";

export abstract class PcdBaseEntity extends BaseEntity {

    @Column({
        name: "created_by",
        type: "varchar",
        length: 12,
        default:() => "'SYSTEM'",
    })
    createdBy: string;

    // @Column({
    //     name: "created_country",
    //     type: "numeric",
    //     nullable: true
    // })
    // createdCountry: string;

    @CreateDateColumn({
        name: "created_date",
        default:() => "CURRENT_TIMESTAMP",
    })
    createdDate: Date;

    @Column({
        name: "modified_by",
        type: "varchar",
        length: 12,
        default:() => "'SYSTEM'",
    })
    modifiedBy: string;

    // @Column({
    //     name: "modified_country",
    //     type: "numeric",
    //     nullable: true
    // })
    // modifiedCountry: string;

    @UpdateDateColumn({
        name: "modified_date",
        default:() => "CURRENT_TIMESTAMP",
    })
    modifiedDate: Date;

    @VersionColumn({
        name: "version_no",
        type: "number",
        nullable: true
    })
    versionNo: number

    @VersionColumn({
        name: "session_no",
        type: "number",
        nullable: true
    })
    sessionNo: number



}