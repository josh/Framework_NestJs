export class PcdBaseDto {

    createdBy: string;

    createdDate: Date;

    modifiedBy: string;

    modifiedDate: Date;

    versionNo: number;

    sessionNo: number;
}