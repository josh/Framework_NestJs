import { BaseEntity, Column } from "typeorm";

export class PcdViewEntity extends BaseEntity {

    @Column("string",{ 
        name:"CREATED_BY"
        })
    createdBy:string;

    @Column("date",{ 
        name:"CREATED_DATE"
        })
    createdDate:Date;
    
}