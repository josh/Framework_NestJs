import { Repository } from "typeorm";

export class TransactionDetailDto {

    detailName: string;

    foreignKeyColumnName?: string;

    foreignKeyAssignmentColumnName?: {columnName:string,headerColumn:string};

    repo: Repository<any>;

    filter: { columnName: string, value?: any, headerColumn?: string }[];

    childDetail: TransactionDetailDto[];

    addtionalSettingOfRowsInInsertion?: { columnName: string, value?: any, headerColumn?: string }[];

    transactionTypeColumnName?:string;

    orderByColumn?: {columnName : string, sort : "DESC" | "ASC" };

}
