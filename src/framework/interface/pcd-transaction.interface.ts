import { PcduserDto,} from "src/auth/dto/pcd-user.dto";
import { FindManyOptions } from "typeorm";

export interface PcdtTransactionInterface {

    findList(filter: FindManyOptions, user: PcduserDto): Promise<any[]>;

}