import { FindManyOptions } from "typeorm";
import { PcduserDto } from "src/auth/dto/pcd-user.dto";

export interface PcdFormGridInterface {

    findAll(filter: FindManyOptions, user: PcduserDto): Promise<any[]>;

    findOne(filter: FindManyOptions, id: string | number, user: PcduserDto): Promise<any>;

    createOne(body: any, user: PcduserDto): Promise<any>

    updateOne(body: any, id: string | number, user: PcduserDto): Promise<any>;

    deleteOne(filter: FindManyOptions, id: string | number, user: PcduserDto): Promise<any>;

}