import { Logger, Get, Query, Delete, Post, Body, Param, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { AuthModule } from "src/auth/auth.module";
import { FindManyOptions } from "typeorm";
import { PcdTransactionService } from "./pcd-transaction.service";
import { GetUser } from "src/auth/dto/get-user.decorator";
import { PcduserDto } from "src/auth/dto/pcd-user.dto";

export class PcdTransactionController<T> {

    private logger = new Logger('PcdTransactionController'); // for logging

    constructor(
        private service: PcdTransactionService<T>
    ) { }

    @Get()
    async findList(@Query() filter: FindManyOptions,
        @GetUser() user: PcduserDto
    ): Promise<T[]> {
        return await this.service.findList(filter, user);
    }

    @Get('/:id')
    async findOne(@Query() filter: FindManyOptions, @Param('id') id: string,): Promise<T> {
        return await this.service.findOne(filter, id);
    }

    @UseGuards(AuthGuard())
    @Post()
    async createOne(@Body() body: T,
        @GetUser() user: PcduserDto): Promise<T> {
        return await this.service.createOne(body, user);
    }


    @UseGuards(AuthGuard())
    @Delete('/:id')
    async delete(@Query() filter: FindManyOptions, @Param('id') id: string,): Promise<any> {
        return await this.service.deleteOne(filter, id,);
    }
}