import { Body, DefaultValuePipe, Delete, Get, Logger, Param, ParseIntPipe, Post, Put, Query, UsePipes, ValidationPipe } from "@nestjs/common";
import { GetUser } from "src/auth/dto/get-user.decorator";
import { PcduserDto } from "src/auth/dto/pcd-user.dto";
import { FindManyOptions } from "typeorm";
import { PcdFormGridService } from "./pcd-form-grid.service";
import { AuthGuard } from "@nestjs/passport";
import { AuthModule } from "src/auth/auth.module";

export class PcdFormGridController<T>{
    private logger = new Logger('PcdFormGridController'); // for logging
    constructor(
        private service: PcdFormGridService<T>
    ) {
    }

    @Get()
    async findAll(
        @Query() filter: FindManyOptions,
        @GetUser() user: PcduserDto
    ): Promise<any[]> {
        return await this.service.findAll(filter, user);
    }


    @Get('/:id')
    async getOne(
        @Query() filterDto: FindManyOptions,
        @Param('id') id: string,
        @GetUser() user: PcduserDto,
    ): Promise<any> {

        return await this.service.findOne(filterDto, id, user);
    }

    @UsePipes(ValidationPipe)
    @Post()
    async createOne(
        @Body() body: T,
        @GetUser() user: PcduserDto
    ): Promise<any> {

        return await this.service.createOne(body, user);
    }

    @Put('/:id')
    async updateOne(
        @Body(ValidationPipe) body: any,
        @Param('id') id: number,
        @GetUser() user: PcduserDto
    ) {
        return await this.service.updateOne(body, id, user);
    }

    @Delete('/:id')
    async deleteOne(
        @Query() filter: FindManyOptions,
        @Param('id') id: string,
        @GetUser() user: PcduserDto,
    ): Promise<any> {
        return await this.service.deleteOne(filter, id, user);
    }

}