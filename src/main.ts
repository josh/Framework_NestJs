import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as config from 'config';
import { Logger } from '@nestjs/common';
import * as bodyParser from 'body-parser';

async function bootstrap() {
  
  const serverConfig = config.get('server');
  // const whiteIp = config.get('whiteIp')
  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(AppModule, { cors: true });
  const port = process.env.PORT || serverConfig.port; // for logging and config
  app.setGlobalPrefix('api/v1');
  app.enableCors();
  await app.listen(port);

  logger.log(`Application listing on port ${port}`) // for logging
}
// Cluster.register(4, bootstrap);
bootstrap()
