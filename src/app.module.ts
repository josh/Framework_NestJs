import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { AuthModule } from './auth/auth.module';
import { FileUploadModule } from './file-upload/file-upload.module';
import { ScheduleModule } from '@nestjs/schedule';
import { PaymentGatewayModule } from './payment-gateway/payment-gateway.module';
import { DataSource } from 'typeorm';



@Module({
  imports: [
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot(typeOrmConfig),
    FileUploadModule,
    AuthModule,
   PaymentGatewayModule,


  ],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
 }
