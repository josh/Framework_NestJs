export class TenderDetailDto{
    createdBy:               string;
    createdDate:             string;
    modifiedBy:              string;
    modifiedDate:            string;
    versionNo:               number;
    sessionNo:               number;
    id:                      number;
    orgChain:                string;
    tenderRefNo:             string;
    tenderId:                string;
    allowTechEval:           string;
    paymentMode:             number;
    allowWithdraw:           string;
    noOfCovers:              number;
    itemwiseTechEval:        string;
    allowTwoStageBid:        string;
    tenderFee:               number;
    feePayableTo:            string;
    feePayableAt:            string;
    tenderFeeExemption:      number;
    emdAmt:                  number;
    emdPayableTo:            string;
    allowEmdBgOrSt:          string;
    emdPercentage:           number;
    emdPayableAt:            string;
    title:                   string;
    workDesc:                string;
    ndaPreQualification:     string;
    remarks:                 string;
    tenderValue:             number;
    contractType:            string;
    location:                string;
    preBidMeetAdd:           string;
    allowNdaTender:          string;
    productCategory:         number;
    bidValidity:             number;
    pincode:                 number;
    preBidDate:              Date;
    allowPreferencialBidder: string;
    subCategory:             string;
    periodOfWork:            number;
    preBidMeetPlace:         string;
    bidOpeningPlace:         string;
    publishDate:             Date;
    saleStartDate:           Date;
    clarificationStartDate:  Date;
    bidSubStartDate:         Date;
    bidOpenDate:             Date;
    saleEndDate:             Date;
    clarificationEndDate:    Date;
    bidSubEndDate:           Date;
    tenderInvName:           string;
    tenderInvAddess:         string;
    tenderType:              number;
    tenderCategory:          number;
    formOfContract:          number;
   emdFeeType:            string;
    activeYn:                string;
    sendReport : string;
    bidOpenAddress : string
}