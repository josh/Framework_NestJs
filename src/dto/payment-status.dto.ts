import { PcdBaseDto } from "src/framework/custom/pcd-dto.custom";

export class PaymentStatusDto extends PcdBaseDto {

    id: number;

    paymentDate: Date;

    paymentOption: string;

    paymentCardDetails: string;

    status: string;

    type: string

    email: string;

    mobile: string;

    application: number;

    paymentId: number;

    billingAddress1: string;

    billingAddress2: string;

    billingAddress3: string;

    billingAddress4: string;

    amount: number;

    remarks: string;

    txnid: number;

    identifier: string;


}