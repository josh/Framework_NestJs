export class PayuTransactionListDto {

    id: string

    status: string

    key: string

    merchantname: string

    firstname: string

    lastname: string

    addedon: Date

    bank_name: string

    payment_gateway: string

    phone: number | any

    email: string

    transaction_fee: number

    amount: number | any

    discount: number | any

    additional_charges: number | any

    productinfo: string

    error_code: string

    bank_ref_no: number | any

    ibibo_code: string

    mode: string

    ip: string

    card_no: number | any

    cardtype: string

    offer_key: string

    field0: string

    field1: string

    field2: string

    field3: string

    field4: string

    field5: string

    field6: string

    field7: string

    field8: string

    field9: string

    udf1: string

    udf2: string

    udf3: string

    udf4: string

    udf5: string

    address2: string

    city: string

    zipcode: string

    pg_mid: string

    issuing_bank: string

    offer_type: string

    failure_reason: string

    mer_service_fee: string

    mer_service_tax: string

    txnid: string;

    gateway: string;
}