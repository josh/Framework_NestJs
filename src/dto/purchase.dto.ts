
export class PurchaseDto {

    amount: number;

    application: string;

    mobile: string;

    email: string;

    type: string;

    menuId?: string;


}