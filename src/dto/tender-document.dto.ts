export class TenderDocumentDto{

    tenderId:number;

    docName:string;

    description:string;

    docType:string;

    documentSize:number;

    documentCategory:string;

    userId?:string
}