export class MenuDto {

    menuId: number;

    menuName: string;

    moduleId: number;

    menuIcon: string;

    menuOrder: number;

    routerLink: string;

    menuParentId: number;
}
