export class PaymentDto {
  firstname: string;
  lastname: string;
  email: string;
  phone: number;
  amount: number;
  productinfo: string;
  txnid: number;
  surl: string;
  furl: string;
  service_provider: string;

  constructor() {
    this.furl = 'http://localhost:3100/api/v1/payment-gateway/failure';
    this.surl = 'http://localhost:3100/api/v1/payment-gateway/success';

    // this.furl = 'https://flexdata.api.teorainfotech.com/API/V1/payment-gateway/failure';
    // this.surl = 'https://flexdata.api.teorainfotech.com/API/V1/payment-gateway/success';

    this.txnid = this.getRandomInt();
  }

  getRandomInt() {
    return Math.floor(100000 + Math.random() * 900000);
  }
}
