export class PaymentMasterDto {

    userId:string

    tenderId:number

    transId:number

    status?:string

    finalBidAmt?:number

    notificationViewed?:string

    message?:string
}