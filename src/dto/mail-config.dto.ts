export class MailConfigDto {

    smtpUserName: string;

    smtpId: number;

    smtpPassword: string;

    smtpHost: string;

    encryptionType: string;

    smtpPort: string;
}
