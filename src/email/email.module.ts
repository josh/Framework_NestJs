import { Module } from '@nestjs/common';
import { EmailController } from './email.controller';
import { EmailService } from './email.service';
import { HandlebarsAdapter, MailerModule } from '@nest-modules/mailer';
import * as config from 'config';
import { TypeOrmModule } from '@nestjs/typeorm';

const emailConfig = config.get('mail');

@Module({
  imports: [
    MailerModule.forRootAsync({
      useFactory: () => ({
        // remember to turn-on the Less secure app access in the gmail security
        transport: {
                host: 'smtp.gmail.com',
                port: 465,
                secure: true,
                auth: {
                    user: emailConfig.user,
                    pass: emailConfig.pass
                },
                tls: { rejectUnauthorized: false } 
              },
       
      }),
    }),
    TypeOrmModule.forFeature([]),
  ],
  controllers: [EmailController],
  providers: [EmailService],
  exports: [
    MailerModule,
    EmailService
  ]
})
export class EmailModule { }
