import { Injectable ,NotFoundException} from '@nestjs/common';
import { MailerService } from '@nest-modules/mailer';
import * as config from 'config';
import { log } from 'console';
import * as nodemailer from 'nodemailer';
import { createFolders } from 'src/file-upload/file-upload.controller';
const mail= config.get('mail');
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { promisify } from 'util';

@Injectable()
export class EmailService {
    reportArray: any;
    constructor(
        private readonly mailerService: MailerService,
    ) { }
    // documenation : https://www.npmjs.com/package/@nest-modules/mailer


   
async generateReport(report,id) {
  
    const enq = await report;
  
    const doc = new jsPDF('landscape');
    let headers = (['id', 'userId', 'tenderId', 'transId', 'finalBidAmt', 'userName', 'email', 'mobileNumber']);
    
    // Prepare the data in the required format for autoTable
    let data = [];
    enq.forEach((item) => {
      data.push([
        item.id,
        item.userId,
        item.tenderId,
        item.transId,
        item.finalBidAmt,
        item.userName,
        item.email,
        item.mobileNumber
      ]);
    });
  
    // Create the table in the PDF document
   

    autoTable(doc,{
      startY: 1, // Adjust the starting position of the table (Y coordinate)
      head: [headers],
      body: data,
    });
  
    // Save the PDF
    const filePath: string = './uploads/Report';
    createFolders(filePath);
    doc.save(`${filePath}/${id}.pdf`);
    console.log(id);
    const pdfFileName = `${id}.pdf`;
    const filePathReport: string = './uploads/Report/';
  
    // Update pdfStatus
    

 // Send email with attachment
 const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
          user: 'website.tscb@gmail.com',
          pass: 'zubhybaneozurlvy',
        },
        tls: {
          rejectUnauthorized: false,
        },
      });

const mailOptions = {
  from: 'tender.tscb178@gmail.com',
  to: 'tender.tscb178@gmail.com',
  subject: `Generated PDF Report for ID: ${report[0].tenderId}`,
  text: 'Attached PDF report.',
  attachments: [
    {
      filename: pdfFileName,
      path:  `${filePathReport}${pdfFileName}`,
    },
  ],
};

const sendMail = promisify(transporter.sendMail.bind(transporter));

try {
  await sendMail(mailOptions);
  console.log('Email sent successfully');
  return {data : "Email sent successfully"}
} catch (error) {
  console.error('Error sending email:', error);
  return {data : "Error sending email:", error}
}

}


    async sendMail(toMail:string,subject:string,bodyText: string,htmlText:string){
        this.mailerService.sendMail({
            to: toMail, // sender address
            subject: subject, // Subject line
            text: bodyText, // plaintext body
            html: htmlText, // HTML body content
        })
        .then((res) => {
            console.log("==== here==res==", res)  
        })
        .catch((err) => {
            console.log("=====err====", err);
        });
    }
}
