import {  Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { AuthService } from 'src/auth/auth.service';
import { CryptoService } from 'src/auth/crypto.service';
import { PcduserRepository } from 'src/auth/pcduser.repository';
import { EmailModule } from 'src/email/email.module';
import { HttpModule } from '@nestjs/axios';

import {  PaymentGatewayController } from './payment-gateway.controller';
import {  PaymentGatewayService } from './payment-gateway.service';

@Module({
  imports: [

    TypeOrmModule.forFeature([
      

    ]), AuthModule,
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),

    EmailModule,
  ],
  controllers: [PaymentGatewayController],
  providers: [PaymentGatewayService,
    CryptoService,
  ]
})
export class PaymentGatewayModule { }
