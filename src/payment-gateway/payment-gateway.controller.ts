import { Body, Controller, Get, Param, Post, Query, Req, Res, UseGuards } from '@nestjs/common';
import { FreeChargePaymentGatewayService, PaymentGatewayService } from './payment-gateway.service';
import * as config from 'config';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/dto/get-user.decorator';
import { PcduserDto } from 'src/auth/dto/pcd-user.dto'; 
import { PurchaseDto } from 'src/dto/purchase.dto';
import { FindManyOptions } from 'typeorm';

@Controller('payment-gateway')
export class PaymentGatewayController {
    constructor(
        private paymentGatewayService: PaymentGatewayService,
    ) { }

    @Post()
    @UseGuards(AuthGuard())
    createPayment(
        @Req() req,
        @Res() res,
        @GetUser() user: PcduserDto
    ): void {
        // console.log('=============',user);

        try {
            const paymentBody: PurchaseDto = req.body;
            paymentBody.amount = req.body.amount;
            paymentBody.email = req.body.email;
            paymentBody['user'] = user.userId;
            paymentBody.mobile = req.body.mobile;
            paymentBody.application = `${req.body.application}ESC${user.userId}`;
            paymentBody.type = req.body.type;
            this.paymentGatewayService.makePayment(req.body, (error, result) => {
                if (error) {
                    res.status(error.code).send(error)
                    //res.send (error);
                } else { 
                    res.send(result);
                }
            });
        } catch (e) {
            console.log("Exception in creating User Data . ", e);
        }
    }

    @Post('public')
    createPaymentPublic(
        @Req() req,
        @Res() res,
    ): void {
        // console.log('=============',user);

        try {
            const paymentBody: PurchaseDto = req.body;
            paymentBody.amount = req.body.amount;
            paymentBody.email = req.body.email;
            paymentBody['user'] = 'system';
            paymentBody.mobile = req.body.mobile;
            paymentBody.application = req.body.application;
            paymentBody.type = req.body.type;
            paymentBody.menuId = 'PUBLIC';
            this.paymentGatewayService.makePayment(req.body, (error, result) => {
                if (error) {
                    res.status(error.code).send(error)
                    //res.send (error);
                } else {
                    res.send(result);
                }
            });
        } catch (e) {
            console.log("Exception in creating User Data . ", e);
        }
    }

    @Post('success')
  async paymentSuccess(
        @Req() req,
        @Res() res,
    ): Promise<void> {
        console.log("==req====", req.body);
        try {
            const paymentSuccessBody = req.body;
            console.log(paymentSuccessBody);
          const   userId =  paymentSuccessBody.lastname.split('ESC')[1];
          const  tenderId = paymentSuccessBody.lastname.split('ESC')[0];
        
        
            this.paymentGatewayService.paymentSuccess(paymentSuccessBody, (error, result) => {
                const prodInfo = paymentSuccessBody.productinfo.split('ESC');
                console.log(prodInfo, '#####');
                console.log('*************', config.get('payment-gateway'));

                if (error) {
                    let redirectUrl
                        redirectUrl = config.get('url').url + config.get('payment-gateway').failureRedirectUrl + `/${req.body.lastname.split('ESC')[0]}/f`;
                       
                    console.log("==redirectUrl====", redirectUrl);
                    res.redirect(redirectUrl);
                    // res.send(error);
                } else {
                    let redirectUrl
                        redirectUrl = config.get('url').url + config.get('payment-gateway').successRedirectUrl +  `/${req.body.lastname.split('ESC')[0]}/s`;
                        
                    console.log("==redirectUrl====", redirectUrl);
                    res.redirect(redirectUrl);
                }
            });

        } catch (e) {
            console.log("Exception in creating User Data . ", e);
        }
    }

    @Post('failure')
   async paymentFailure(
        @Req() req,
        @Res() res,
    ): Promise<void> {
        console.log(req.body, 'request body');
        try {
            const paymentFailureBody = req.body;
            const   userId =  paymentFailureBody.lastname.split('ESC')[1];
            const  tenderId = paymentFailureBody.lastname.split('ESC')[0];
         
            this.paymentGatewayService.paymentFailure(paymentFailureBody, (error, result) => {
                console.log(paymentFailureBody);
                console.log(config.get('payment-gateway'));

               

                if (error) {
                    res.send(error);
                } else {
                    // Redirect to payment failure page
                    let redirectUrl
                        redirectUrl = config.get('url').url + config.get('payment-gateway').failureRedirectUrl +  `${req.body.lastname.split('ESC')[0]}/f`;
                    res.redirect(redirectUrl);
                }
            });
        } catch (e) {
            console.log("Exception in creating User Data . ", e);
        }
    }
}

// @Controller('payment-gateway-fc')
// export class FreeChargePaymentGatewayController {
//     constructor(
//         private freeChargePaymentGatewayService: FreeChargePaymentGatewayService
//     ) { }

//     @Post('status')
//     async checkPaymentStatus(
//         @Body() body: any,
//         @GetUser() user: PcduserDto
//     ): Promise<any> {
//         console.log("===dd===", body);
//         return await this.freeChargePaymentGatewayService.checkPaymentStatus(body, user);
//     }

//     @Post('refund')
//     async refundPayment(
//         @Body() body: any,
//         @GetUser() user: PcduserDto
//     ): Promise<any> {
//         console.log("===dd===", body);
//         return await this.freeChargePaymentGatewayService.refundPayment(body, user);
//     }


//     @Post()
//     @UseGuards(AuthGuard())
//     createPayment(
//         @Req() req,
//         @Res() res,
//         @GetUser() user: PcduserDto
//     ): void {
//         // console.log('=============',user);

//         try {
//             const paymentBody: PurchaseDto = req.body;
//             paymentBody.amount = req.body.amount;
//             paymentBody.email = req.body.email;
//             paymentBody['user'] = user.userId;
//             paymentBody.mobile = req.body.mobile;
//             paymentBody.application = req.body.application;
//             paymentBody.type = req.body.type;
//             this.freeChargePaymentGatewayService.makePayment(paymentBody, (error, result) => {
//                 if (error) {
//                     // console.log(error,'err here');

//                     res.status(error.status).send()
//                     //res.send (error);
//                 } else {
//                     res.send(result);
//                 }
//             });
//         } catch (e) {
//             console.log("Exception in creating User Data . ", e);
//         }
//     }


//     @Post('success')
//     paymentSuccess(
//         @Req() req,
//         @Res() res,
//     ): void {
//         console.log("==req====", req.body);
//         try {

//             console.log(req.body);
//             if (req.body.statusCode == "SPG-0000") {
//                 const paymentSuccessBody = req.body;
//                 this.freeChargePaymentGatewayService.paymentSuccess(paymentSuccessBody, (error, result) => {
//                     if (error) {
//                         console.log(error, 'error test');

//                         let redirectUrl
//                         if (paymentSuccessBody.productinfo == 'F') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').successRedirectUrl + `/${paymentSuccessBody.lastname}?payment=Y`;
//                         } else if (paymentSuccessBody.productinfo == 'S') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').failureRedirectUrlStudent + `?id=${paymentSuccessBody.lastname}&status=Y`;
//                         } else if (paymentSuccessBody.productinfo == 'A') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').failureRedirectUrlAcceptance + `/${paymentSuccessBody.lastname}?status=N`;
//                         } else if (paymentSuccessBody.productinfo == 'I') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').failureRedirectUrlStudentInternal + `?rollNo=${paymentSuccessBody.lastname}&status=N`;
//                         }


//                         res.redirect(redirectUrl);
//                         // res.send(error);
//                     } else {
//                         console.log('success test', paymentSuccessBody.productinfo);

//                         let redirectUrl
//                         if (paymentSuccessBody.productinfo == 'F') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').successRedirectUrl + `/${paymentSuccessBody.lastname}?payment=Y`;
//                         } else if (paymentSuccessBody.productinfo == 'S') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').successRedirectUrlStudent + `?id=${paymentSuccessBody.lastname}&status=Y`;
//                         } else if (paymentSuccessBody.productinfo == 'A') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').successRedirectUrlAcceptance + `/${paymentSuccessBody.lastname}?status=Y`;
//                         } else if (paymentSuccessBody.productinfo == 'I') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').successRedirectUrlStudentInternal + `?rollNo=${paymentSuccessBody.lastname}&studentId=${paymentSuccessBody.studentId}&status=Y`;
//                         }
//                         console.log(redirectUrl, 'red');
//                         res.redirect(redirectUrl);
//                     }
//                 });
//             }
//             else if (req.body.statusCode == "SPG-0001") {
//                 const paymentFailureBody = req.body;
//                 this.freeChargePaymentGatewayService.paymentFailure(paymentFailureBody, (error, result) => {
//                     if (error) {
//                         res.send(error);
//                     } else {
//                         // Redirect to payment failure page
//                         let redirectUrl
//                         if (paymentFailureBody.productinfo.split('ESC')[1] == 'F') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').successRedirectUrl + `/${paymentFailureBody.lastname}?payment=N`;
//                         } else if (paymentFailureBody.productinfo.split('ESC')[1] == 'S') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').failureRedirectUrlStudent + `?id=${paymentFailureBody.lastname}&status=N`;
//                         } else if (paymentFailureBody.productinfo.split('ESC')[1] == 'A') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').failureRedirectUrlAcceptance + `/${paymentFailureBody.lastname}?status=N`;
//                         } else if (paymentFailureBody.productinfo.split('ESC')[1] == 'I') {
//                             redirectUrl = config.get('url').url + config.get('payment-gateway').failureRedirectUrlStudentInternal + `?rollNo=${paymentFailureBody.lastname}&status=N`;
//                         }

//                         res.redirect(redirectUrl);
//                     }
//                 });
//             }
//         } catch (e) {
//             console.log("Exception in creating User Data . ", e);
//         }
    // }


// }
