import { BadRequestException, HttpException, Injectable, InternalServerErrorException, Logger, MethodNotAllowedException } from '@nestjs/common';
import { PaymentDto } from '../dto/payment.dto';
import { AuthService } from 'src/auth/auth.service';
import { FindManyOptions } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
//import { generate } from 'generate-password';
import * as config from 'config';
import { CryptoService } from 'src/auth/crypto.service';
import { PcduserRepository } from 'src/auth/pcduser.repository';
import { PurchaseDto } from 'src/dto/purchase.dto';
import { PaymentStatusDto } from 'src/dto/payment-status.dto';
import { convertDate } from 'src/pipe/pcd-date.pipe';
var crypto = require('crypto')

import { PayuTransactionListDto } from 'src/dto/payu-transaction-list.dto';
import { PcduserDto } from 'src/auth/dto/pcd-user.dto';
import { PaymentMasterDto } from 'src/dto/payment-master.dto';
import { Console } from 'console';
import { HttpService } from '@nestjs/axios';



const payUMoney = require("payumoney_nodejs");
const querystring = require('querystring');

const paymentGatewayConf = config.get('payment-gateway');
const paymentGatewayMethod: "DHOFAR" | "PAYU" = paymentGatewayConf.method;

@Injectable()
export class PaymentGatewayService {

    private logger = new Logger('PaymentGatewayService'); // for logging


    constructor(
        private readonly http: HttpService,
        // private authService: AuthService,
      
        private crypto: CryptoService,
      
    ) {
        // Set you MERCHANT_KEY, MERCHANT_SALT_KEY, PAYUMONEY_AUTHORIZATION_HEADER
        // for both Production and Sandox Account
        payUMoney.setProdKeys(paymentGatewayConf.key, paymentGatewayConf.salt, "ylb6+TCDC20wlyTooZbYM9wDw4abjaOVV9KiHFzGPgw=");
        payUMoney.setSandboxKeys(paymentGatewayConf.key, paymentGatewayConf.salt, "ylb6+TCDC20wlyTooZbYM9wDw4abjaOVV9KiHFzGPgw=");
        payUMoney.isProdMode(true);
    }

    public async makePayment(paymentBody: PurchaseDto, callback: (error: any, response: any) => void) {
        // let total = 0;
        // let tax = 0;
        // let totalAmount = 0;
        // let minAmount = paymentBody.amount;
        // key : "crvbe4"
        // salt : "wIVuIkieIBBrSYtXP3aVw5uFH7eFe2nM"

        let date = new Date()
        let paymentStatus = new PaymentStatusDto()
        paymentStatus.createdBy = paymentBody["user"];
        paymentStatus.modifiedBy = paymentBody["user"];
        paymentStatus.email = paymentBody.email;
        paymentStatus.type = paymentBody.type;
        paymentStatus.status = 'U';
        paymentStatus.paymentOption = 'P'; //F - FreeCharge | P - PayU 

        let paymentModel = new PaymentDto();
        paymentModel.txnid = Date.now();
        paymentStatus.txnid = paymentModel.txnid;
        paymentModel.firstname = 'Admin';
        paymentModel.lastname = paymentBody.application.toString();
        paymentModel.email = paymentBody.email;
        paymentModel.service_provider = 'payu_paisa';
        paymentModel.furl = config.get('payment-gateway').furl;
        paymentModel.surl = config.get('payment-gateway').surl;
        paymentModel.amount = 1;

        let paymentMaster = new PaymentMasterDto();
        paymentMaster.userId =  paymentModel.lastname.split('ESC')[1];
        paymentMaster.tenderId =parseInt( paymentModel.lastname.split('ESC')[0]);
        paymentMaster.transId = paymentStatus.txnid;
        paymentMaster.status = 'P';
        console.log('paymentmasterDto======',paymentMaster);
        


        // if (paymentBody.type == 'F') {
        //     let application = await this.sisAdmissionFormRepository.findOne(paymentBody.application);
        //     if (!application)
        //         throw new BadRequestException('application not generated')
        //     paymentStatus.paymentDate = new Date();
        //     paymentStatus.amount = application.receiptAmount;
        //     paymentStatus.application = application.id;
        //     paymentModel.phone = parseInt(application.motherPhoneNo);
        //     if (paymentGatewayConf.testing == true)
        //         paymentModel.amount = 1;
        //     else
        //         paymentModel.amount = application.receiptAmount;
        // }
        // if (paymentBody.type == 'S') {
        //     let payment = await this.sisFeePaymentOnlineRepository.findOne(paymentBody.application);
        //     if (!payment)
        //         throw new BadRequestException('payment not generated')
        //     paymentStatus.paymentDate = new Date();
        //     paymentStatus.amount = payment.paidAmount;
        //     paymentStatus.paymentId = payment.id;
        //     paymentModel.phone = payment.contactPhone;
        //     if (paymentGatewayConf.testing == true)
        //         paymentModel.amount = 1;
        //     else
        //         paymentModel.amount = payment.paidAmount;

        // }
        // if (paymentBody.type == 'I') {
        //     let payment = await this.sisFeePaymentOnlineRepository.findOne(paymentBody.application);
        //     if (!payment)
        //         throw new BadRequestException('payment not generated')
        //     paymentStatus.paymentDate = new Date();
        //     paymentStatus.amount = payment.paidAmount;
        //     paymentStatus.paymentId = payment.id;
        //     paymentModel.phone = payment.contactPhone;
        //     if (paymentGatewayConf.testing == true)
        //         paymentModel.amount = 1;
        //     else
        //         paymentModel.amount = payment.paidAmount;

        // }
        // if (paymentBody.type == 'A') {
        //     let application = await this.sisAdmissionFormRepository.findOne(paymentBody.application);
        //     let courseFee = await this.sisFeeRepository.findOne({ where: { id: application.acceptance } })
        //     if (!application)
        //         throw new BadRequestException('payment not generated')
        //     let date = new Date()
        //     paymentStatus.paymentDate = new Date();
        //     paymentStatus.amount = parseInt(courseFee.feeCharge);
        //     paymentStatus.application = application.id;
        //     paymentStatus.identifier = 'A';
        //     paymentModel.phone = parseInt(application.motherPhoneNo);
        //     if (paymentGatewayConf.testing == true)
        //         paymentModel.amount = 1;
        //     else
        //         paymentModel.amount = parseInt(courseFee.feeCharge);

        // }
        // paymentModel.productinfo = `${pay.id}ESC${paymentBody.type}`;
        console.log(paymentModel);

        {
            payUMoney.pay(paymentModel, (error: any, response: any) => {
                if (error) {
                    console.error("makePayment error : " + error);
                    callback(error, null);
                } else {
                    console.log("Payment Redirection link " + response + '===' + error);
                    callback(error, { url: response });
                }
            });
        }
    }

    public async paymentSuccess(paymentBody: any, callback: (error: any, response: any) => void) {
        // console.log("===payment body===", paymentBody);
        let code;
        let paymentSuccessBody;
        let receiptNo;
        let receiptNoPrefix;

        paymentSuccessBody = paymentBody;
        code = paymentSuccessBody.txnid;
        console.log(paymentSuccessBody, 'successbody');

        // let paymentStatus = await this.paymentStatusRepo.findOne({ where: { "id": paymentSuccessBody.productinfo.split('ESC')[0] } });
        // paymentStatus.status = 'S';
        // await this.paymentStatusRepo.save(paymentStatus); 

        // const queryRunner = getConnection().createQueryRunner();
        // await queryRunner.connect();
        // await queryRunner.startTransaction();
        // try {
        //     let productData
        //     // if (paymentStatus.type == 'F' || paymentStatus.type == 'A') {
        //     //     productData = await this.sisAdmissionFormRepository.findOne({ where: { id: paymentStatus.application }, select: ['id', 'sessionId'] })
        //     // } else {
        //     //     productData = await this.sisFeePaymentOnlineRepository.findOne({ where: { id: paymentStatus.paymentId }, select: ['id', 'sessionId'] })
        //     // }

        //     // const session = await this.sisSessionRepository.findOne({ where: { id: productData.sessionId } });
        //     // receiptNoPrefix = session.sessionCode;
        //     // receiptNo = parseInt(session.receiptSeq.toString()) + 1;

        // //     await queryRunner.manager.query(`update sis_session set receipt_seq = ${receiptNo} where id = ${session.id}`);
        // //     if (paymentStatus.type == 'F')
        // //         await queryRunner.manager.query(`update sis_admission_form set payment_stat ='S',receipt_no_prefix='${receiptNoPrefix}',receipt_no = '${receiptNo}' where id = ${paymentStatus.application}`)
        // //     if (paymentStatus.type == 'S') {
        // //         await queryRunner.manager.query(`update sis_fee_payment_online set payment_stat ='S',txnid = '${code}',receipt_no_prefix='${receiptNoPrefix}',receipt_no = ${receiptNo} where id = ${paymentStatus.paymentId}`)
        // //     }
        // //     if (paymentStatus.type == 'I') {
        // //         await queryRunner.manager.query(`update sis_fee_payment_online set payment_stat ='S',txnid = '${code}',receipt_no_prefix='${receiptNoPrefix}',receipt_no = ${receiptNo} where id = ${paymentStatus.paymentId}`)
        // //         const data = await this.sisFeePaymentOnlineRepository.findOne({ where: { id: paymentStatus.paymentId }, select: ['id', 'studentId'] });
        // //         const student = await this.sisStudentRepository.findOne({ where: { id: data.studentId }, select: ['id', 'rollNo', 'sessionId'] });
        // //         paymentBody.lastname = student.rollNo;
        // //         paymentBody['studentId'] = student.id;
        // //         paymentBody['sessionId'] = student.sessionId;

        // //     }
        // //     if (paymentStatus.type == 'A') {
        // //         await queryRunner.manager.query(`update sis_admission_form set acceptance_fee_stat ='S',acceptance_receipt_no_prefix='${receiptNoPrefix}',acceptance_receipt_no = '${receiptNo}' where id = ${paymentStatus.application}`)
        // //     }
        //     queryRunner.commitTransaction()


            callback(null, { status: "Payment Success", id: code });
        // } catch (error) {
        //     queryRunner.rollbackTransaction()
        // } finally {
        //     queryRunner.release()
        // }

    }

    public async paymentFailure(paymentBody: any, callback: (error: any, response: any) => void) {
        let paymentFailureBody;
        let code;
        console.log("Payment Failure");
        paymentFailureBody = paymentBody;


        // let paymentStatus = await this.paymentStatusRepo.findOne({ where: { "txnid": paymentFailureBody.txnid } });
        // paymentStatus.status = 'F';
        // await this.paymentStatusRepo.save(paymentStatus);

        code = paymentFailureBody.txnid;

        // await this.authService.updateHeader(code, 2, paymentFailureBody);
        callback(null, { status: "Payment Failed" });
    }

    async insertPaymentStatus() {

    }

    // PAYU SECTION END DONT ADD PAYU RELATED CODE BELOW



}
@Injectable()
export class FreeChargePaymentGatewayService {

    constructor(
        private readonly http: HttpService,
        private crypto: CryptoService,
    ) { }

    async getUrl() {
        if (config.get('payment-gateway-freecharge').production) {
            return config.get('payment-gateway-freecharge').prodUrl;
        } else {
            return config.get('payment-gateway-freecharge').sandUrl;
        }
    }

    async makePayment(paymentBody: PurchaseDto, callback: (error: any, response: any) => void) {
        let date = new Date()

        let paymentStatus = new PaymentStatusDto()
        paymentStatus.createdBy = paymentBody["user"];
        paymentStatus.modifiedBy = paymentBody["user"];
        paymentStatus.email = paymentBody.email;
        paymentStatus.type = paymentBody.type;
        paymentStatus.status = 'U';
        paymentStatus.paymentOption = 'F'; //F - FreeCharge | P - PayU 


        let paymentJson = {
            merchantId: config.get('payment-gateway-freecharge').merchantId,
            callbackUrl: config.get('payment-gateway-freecharge').callBackUrl,
            merchantTxnId: null,
            merchantTxnAmount: paymentBody.amount,
            currency: config.get('payment-gateway-freecharge').currency,
            customerEmailId: paymentBody.email,
            customerMobileNo: paymentBody.mobile,
            timestamp: date.getTime(),
            udf1: null
        }
        paymentStatus.txnid = paymentJson.timestamp;
        paymentJson.merchantTxnId = paymentJson.timestamp;

        // if (paymentBody.type == 'F') {
        //     let application = await this.sisAdmissionFormRepository.findOne(paymentBody.application);
        //     if (!application)
        //         throw new BadRequestException('application not generated')
        //     paymentStatus.paymentDate = new Date();
        //     paymentStatus.amount = application.receiptAmount;
        //     paymentStatus.application = application.id;
        //     paymentJson.customerMobileNo = application.motherPhoneNo;
        //     if (paymentGatewayConf.testing == true)
        //         paymentJson.merchantTxnAmount = 1;
        //     else
        //         paymentJson.merchantTxnAmount = application.receiptAmount;
        // }
        // if (paymentBody.type == 'S') {
        //     let payment = await this.sisFeePaymentOnlineRepository.findOne(paymentBody.application);
        //     if (!payment)
        //         throw new BadRequestException('payment not generated')
        //     paymentStatus.paymentDate = new Date();
        //     paymentStatus.amount = payment.paidAmount;
        //     paymentStatus.paymentId = payment.id;
        //     paymentJson.customerMobileNo = payment.contactPhone.toString();
        //     if (paymentGatewayConf.testing == true)
        //         paymentJson.merchantTxnAmount = 1;
        //     else
        //         paymentJson.merchantTxnAmount = payment.paidAmount;

        // }
        // if (paymentBody.type == 'I') {
        //     let payment = await this.sisFeePaymentOnlineRepository.findOne(paymentBody.application);
        //     if (!payment)
        //         throw new BadRequestException('payment not generated')
        //     paymentStatus.paymentDate = new Date();
        //     paymentStatus.amount = payment.paidAmount;
        //     paymentStatus.paymentId = payment.id;
        //     paymentJson.merchantTxnAmount = payment.contactPhone;
        //     if (paymentGatewayConf.testing == true)
        //         paymentJson.merchantTxnAmount = 1;
        //     else
        //         paymentJson.merchantTxnAmount = payment.paidAmount;

        // }
        // if (paymentBody.type == 'A') {
        //     let application = await this.sisAdmissionFormRepository.findOne(paymentBody.application);
        //     let courseFee = await this.sisFeeRepository.findOne({ where: { id: application.acceptance } })
        //     if (!application)
        //         throw new BadRequestException('payment not generated')
        //     let date = new Date()
        //     paymentStatus.paymentDate = new Date();
        //     paymentStatus.amount = parseInt(courseFee.feeCharge);
        //     paymentStatus.application = application.id;
        //     paymentStatus.identifier = 'A';
        //     paymentJson.customerMobileNo = application.motherPhoneNo;
        //     if (paymentGatewayConf.testing == true)
        //         paymentJson.merchantTxnAmount = 1;
        //     else
        //         paymentJson.merchantTxnAmount = parseInt(courseFee.feeCharge);

        // }
        // const pay = await this.paymentStatusRepo.save(paymentStatus);
        // paymentJson.udf1 = `${pay.id}ESC${paymentBody.type}`;

        const ordered = Object.keys(paymentJson).sort().reduce(
            (obj, key) => {
                obj[key] = paymentJson[key];
                return obj;
            },
            {}
        );

        console.log(ordered)

        let string = ''

        for (const [key, value] of Object.entries(ordered)) {
            if (value)
                string = string + value
        }
        string = string + config.get('payment-gateway-freecharge').key
        console.log(string);

        const hash = crypto.createHash('sha256').update(string).digest('hex');

        console.log(hash);
        paymentJson['signature'] = hash
        callback(null, { paymentBody: paymentJson, url: `${await this.getUrl()}/payment/v1/checkout` })
    }

    public async paymentSuccess(paymentBody: any, callback: (error: any, response: any) => void) {
        // console.log("===payment body===", paymentBody);
        let code;
        let paymentSuccessBody;
        let receiptNo;
        let receiptNoPrefix;

        paymentSuccessBody = paymentBody;

        console.log(paymentSuccessBody, 'successbody');


        // // let paymentStatus = await this.paymentStatusRepo.findOne({ where: { txnid: paymentBody.merchantTxnId } });
        // await this.insertPaymentRecord(paymentStatus, paymentSuccessBody);
        // if (paymentStatus.application) {
        //     paymentSuccessBody.lastname = paymentStatus.application;
        // } else if (paymentStatus.paymentId) {
        //     paymentSuccessBody.lastname = paymentStatus.paymentId
        // }
        // code = paymentStatus.txnid;
        // paymentStatus.status = 'S';
        // paymentStatus.txnrefid = paymentSuccessBody.txnReferenceId;
        // paymentSuccessBody.productinfo = paymentStatus.type;
        // await this.paymentStatusRepo.save(paymentStatus);

        // const queryRunner = getConnection().createQueryRunner();
        // await queryRunner.connect();
        // await queryRunner.startTransaction();
        // try {
        //     let productData
        //     // if (paymentStatus.type == 'F' || paymentStatus.type == 'A') {
        //     //     productData = await this.sisAdmissionFormRepository.findOne({ where: { id: paymentStatus.application }, select: ['id', 'sessionId'] })
        //     // } else {
        //     //     productData = await this.sisFeePaymentOnlineRepository.findOne({ where: { id: paymentStatus.paymentId }, select: ['id', 'sessionId'] })
        //     // }

        //     // const session = await this.sisSessionRepository.findOne({ where: { id: productData.sessionId } });
        //     // receiptNoPrefix = session.sessionCode;
        //     // receiptNo = parseInt(session.receiptSeq.toString()) + 1;

        //     // await queryRunner.manager.query(`update sis_session set receipt_seq = ${receiptNo} where id = ${session.id}`);
        //     // if (paymentStatus.type == 'F')
        //     //     await queryRunner.manager.query(`update sis_admission_form set payment_stat ='S',receipt_no_prefix='${receiptNoPrefix}',receipt_no = '${receiptNo}' where id = ${paymentStatus.application}`)
        //     // if (paymentStatus.type == 'S') {
        //     //     await queryRunner.manager.query(`update sis_fee_payment_online set payment_stat ='S',txnid = '${code}',receipt_no_prefix='${receiptNoPrefix}',receipt_no = ${receiptNo} where id = ${paymentStatus.paymentId}`)
        //     // }
        //     // if (paymentStatus.type == 'I') {
        //     //     await queryRunner.manager.query(`update sis_fee_payment_online set payment_stat ='S',txnid = '${code}',receipt_no_prefix='${receiptNoPrefix}',receipt_no = ${receiptNo} where id = ${paymentStatus.paymentId}`)
        //     //     const data = await this.sisFeePaymentOnlineRepository.findOne({ where: { id: paymentStatus.paymentId }, select: ['id', 'studentId'] });
        //     //     const student = await this.sisStudentRepository.findOne({ where: { id: data.studentId }, select: ['id', 'rollNo', 'sessionId'] });
        //     //     paymentBody.lastname = student.rollNo;
        //     //     paymentBody['studentId'] = student.id;
        //     //     paymentBody['sessionId'] = student.sessionId;

        //     // }
        //     // if (paymentStatus.type == 'A') {
        //     //     await queryRunner.manager.query(`update sis_admission_form set acceptance_fee_stat ='S',acceptance_receipt_no_prefix='${receiptNoPrefix}',acceptance_receipt_no = '${receiptNo}' where id = ${paymentStatus.application}`)
        //     // }
        //     queryRunner.commitTransaction()


        //     callback(null, { status: "Payment Success", id: code });
        // } catch (error) {
        //     console.log(error);

        //     queryRunner.rollbackTransaction()
        // } finally {
        //     queryRunner.release()
        // }

    }

    async insertPaymentRecord(paymentStatus: PaymentStatusDto, paymentSuccessBody: any) {
        let PayuTransaction = new PayuTransactionListDto();
        PayuTransaction.addedon = paymentStatus.paymentDate;
        PayuTransaction.amount = paymentSuccessBody.amount;
        PayuTransaction.txnid = paymentSuccessBody.merchantTxnId;
        PayuTransaction.status = 'success';
        PayuTransaction.gateway = 'FreeCharge';
        PayuTransaction.transaction_fee = paymentSuccessBody.amount;
        PayuTransaction.mer_service_fee = paymentSuccessBody.handlingFee;
        PayuTransaction.mer_service_tax = paymentSuccessBody.taxAmount;
        PayuTransaction.id = paymentSuccessBody.txnReferenceId;
        // await this.PayuTransactionListRepository.save(PayuTransaction)
    }


    async checkPaymentStatus(body: any, user: PcduserDto) {
        let paymentBody = {
            "merchantId": `${config.get('payment-gateway-freecharge').merchantId}`,
            "merchantTxnId": `${body.merchantTxnId}`
        }
        let string = ''
        for (const [key, value] of Object.entries(paymentBody)) {
            if (value)
                string = string + value
        }
        string = string + config.get('payment-gateway-freecharge').key
        console.log(string);

        const hash = crypto.createHash('sha256').update(string).digest('hex');
        paymentBody['signature'] = hash;
        return this.http.post(`${await this.getUrl()}/payment/v1/txn/status`, paymentBody).toPromise().then((res) => {
            console.log("===res====", res.data);
            return res.data;
        }, (err) => {
            console.log("===errr====", err);
            return err;
        });
    }

    async refundPayment(body: any, user: PcduserDto) {
        let date = new Date()
        let paymentBody = {
            "merchantId": `${config.get('payment-gateway-freecharge').merchantId}`,
            "merchantRefundTxnId": date.getTime(),
            "txnReferenceId": body.txnReferenceId,
            "refundAmount": body.amount,
            "currency": "INR"
        }
        const ordered = Object.keys(paymentBody).sort().reduce(
            (obj, key) => {
                obj[key] = paymentBody[key];
                return obj;
            },
            {}
        );
        let string = ''
        for (const [key, value] of Object.entries(ordered)) {
            if (value)
                string = string + value
        }
        string = string + config.get('payment-gateway-freecharge').key;
        const hash = crypto.createHash('sha256').update(string).digest('hex');
        paymentBody['signature'] = hash;
        return this.http.post(`${await this.getUrl()}/payment/v1/refund`, paymentBody).toPromise().then((res) => {
            console.log("===res====", res.data);
            return res.data;
        }, (err) => {
            console.log("===errr====", err);
            return err;
        });
    }

    public async paymentFailure(paymentBody: any, callback: (error: any, response: any) => void) {
        let paymentFailureBody;
        let code;

        paymentFailureBody = paymentBody;
        // let paymentStatus = await this.paymentStatusRepo.findOne({ where: { "txnid": paymentFailureBody.merchantTxnId } });
        // paymentFailureBody.productinfo = paymentStatus.type;
        // if (paymentStatus.application) {
        //     paymentFailureBody.lastname = paymentStatus.application;
        // } else if (paymentStatus.paymentId) {
        //     paymentFailureBody.lastname = paymentStatus.paymentId
        // }

        // paymentStatus.status = 'F';
        // await this.paymentStatusRepo.save(paymentStatus);

        code = paymentFailureBody.merchantTxnId;

        // await this.authService.updateHeader(code, 2, paymentFailureBody);
        callback(null, { status: "Payment Failed" });
    }


}



