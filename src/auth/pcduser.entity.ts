import { PcdBaseEntity } from "src/framework/custom/pcd-entity.custom";
import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import * as bcrypt from 'bcrypt';
import { BadRequestException } from "@nestjs/common";

@Entity('pcduser')
export class Pcduser extends PcdBaseEntity {
    @PrimaryColumn({
        name: "user_id",
        primary: true,
        type: "varchar",
        length: 12,
    })
    userId: string;
    @Column({
        name: "user_name",
        type: "varchar",
        length: 100
    })
    userName: string;

    @Column({
        name: "group_or_user",
        type: "char",
        length: 1,
        default: () => "'U'",
    })
    groupOrUser: string;
    @Column({
        name: "pass",
        type: "varchar",
        length: 500
    })
    pass: string | null;


    @Column({
        name: "salt",
        type: "varchar",
        nullable: true,
        length: 50
    })
    salt: string | null;


    @Column({
        name: "secu_level",
        type: "char",
        nullable: true,
        length: 1
    })
    secuLevel: string | null;

    @Column({
        name: "pwd_exp_days",
        type: "int",
        nullable: true,
        default: () => "(0)"
    })
    pwdExpDays: number | null;

    @Column({
        name: 'email',
        type: 'varchar',
        length: 500,
        // unique: true,
        nullable: true
    })
    email: string;

    @Column({
        name: 'mobile_number',
        type: 'varchar',
        length: 100,
        nullable: true,
        unique: false
    })
    mobileNumber: string;

    @Column({
        name: 'refresh_token',
        type: 'varchar',
        nullable: true,
        length: 100,
    })
    refreshtoken: string;

    @Column({
        name: 'user_image_path',
        type: 'varchar',
        length: 500,
        nullable: true
    })
    userImagePath: string;

    @Column({
        name: 'member_no',
        type: 'varchar',
        length: 20,
        nullable: true
    })
    memberNo: string;

    @Column({
        type: 'varchar',
        name: "cust_id",
        nullable: true,
        length: 20,
    })
    custId: string;
   
    @Column({
        name: "device_id",
        type: "varchar",
        nullable: true,
    })
    deviceId:string;


    @Column({
        type: 'char',
        name: "status",
        nullable: true,
        length: 1
    })
    status: string;
    @Column({
        type: 'varchar',
        name: "bank_id",
        nullable: true,
        unique: true,
        length: 20,
    })
    bankId: string;
    @Column({
        nullable:true,
        name: "t_pass",
        type: "varchar",
        length: 500
    })
    tPass: string | null;


    @Column({
        name: "t_salt",
        type: "varchar",
        nullable: true,
        length: 50
    })
    tSalt: string | null;

    async validatePassword(password: string,user ): Promise<boolean> {
            const hash = await bcrypt.hash(password, this.salt);
            return hash === this.pass;
    }

    // async validateTransactionPassword(password: string): Promise<boolean> {
    //     const hash = await bcrypt.hash(password, this.tSalt);
    //     return hash === this.tPass;

    // }

 
}