import { Entity, PrimaryColumn, Column, PrimaryGeneratedColumn } from "typeorm";
import { PcdBaseEntity } from "../framework/custom/pcd-entity.custom";

@Entity('pcdotp')
export class Pcdotp extends PcdBaseEntity {

    @PrimaryGeneratedColumn({
        name: 'code',
    })
    code: number;

    @Column({
        type: 'varchar',
        name: 'otp',
        length: 6,
        nullable: false,
    })
    otp: string;

    @Column({
        type: 'varchar',
        name: 'ref_code',
        nullable: true,
    })
    refCode: string;

    @Column({
        type: 'varchar',
        name: 'used_for',
        length: 500,
        nullable: true,
    })
    usedFor: string;

    @Column({
        type: 'char',
        name: 'status',
        length: 1,
        nullable: false,
    })
    status: string;
}