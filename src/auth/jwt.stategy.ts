import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Injectable, UnauthorizedException, Logger, Inject, Scope } from '@nestjs/common';
import { JwtPayload } from './jwt-payload.interface';
import * as config from 'config';
import { PcduserRepository } from './pcduser.repository';
import { PcduserDto } from 'src/auth/dto/pcd-user.dto';
import { DataSource } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    private logger = new Logger('Jwt Strategy'); // for logging
    constructor(
        @InjectRepository(PcduserRepository)
        private pcduserRepository: PcduserRepository,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: true,
            secretOrKey: process.env.JWT_SECRET || config.get('jwt').secret,
        })
    }

    async validate(payload: JwtPayload) {
        const { userId, userName, emailId, module } = payload;
        this.logger.log(`Payload ${JSON.stringify(payload)}`);
    
        const repo = await this.pcduserRepository;
        const user:PcduserDto = await repo.findOne({where:{userId:userId}});
        delete user.pass;
        delete user.salt;
        if (!user) {
            this.logger.log(` Unauthorized `);
            throw new UnauthorizedException();
        }
           
        user["payload"] = payload;

        return user;
    }
    
}