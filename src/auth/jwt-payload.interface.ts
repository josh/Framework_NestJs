export interface JwtPayload {

    userId: string;

    emailId: string;

    userName: string;

    sessionId: string;

    module: string;

}