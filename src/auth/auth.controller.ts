import { Controller, Get, UseGuards, Res, Req, Body, ValidationPipe, Post, HttpCode, Ip, HostParam, HttpStatus, Put, Param, Patch } from '@nestjs/common';

import { TokenDto } from './dto/token.dto';

import * as config from 'config';
import { AuthCreadentialsDto } from './dto/auth-credentials.dto';
import { AuthService } from './auth.service';
import { TempSignUpDto } from './dto/temp-sign-up.dto';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from './dto/get-user.decorator';
import { Pcduser } from './pcduser.entity';
import { PcduserDto } from './dto/pcd-user.dto';
import { Cron } from '@nestjs/schedule';
import { ChangePasswordDto } from './dto/change-password.dto';
import { UserBankDto } from './dto/user-bank.dto';

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService
    ) { 
    }

    @Post('/user-info')
    async userInfo(@Body() authCredentialDto: AuthCreadentialsDto) {
        return this.authService.userInfo(authCredentialDto)
    }


    @Post('/signin')
    async signIn(@Body(ValidationPipe)
    authCredentialDto: AuthCreadentialsDto,
        @Req() req): Promise<TokenDto> {
        return this.authService.signIn(authCredentialDto, req.ip);
    }

    @Get()
    async getall() {
        return this.authService
    }

    @Post('/signup')
    async signUp(@Body() pcdUserDto: PcduserDto): Promise<any> {
        // request.connection.remoteAddress
        return this.authService.tempSignUp(pcdUserDto);
    }

    @Post('/forgot-password')
    async forgotPassword(
        @Body() body: { pass: string, email: string, otp: string },
    ): Promise<any> {
        return await this.authService.forgotPassword(body);
    }

    @Post('/otp/:flag')
    async sendOtp(
        @Body() body: { email: string, custName: string, tenderName?: string, bidOpenDate?: string },
        @Param('flag') flag: string
    ): Promise<void> {
        console.log("helo==========================");
        
        return await this.authService.sendOtp(body, flag);
    }

    // @Post('/resend-otp/:flag')
    // async resendOtp(
    //     @Body() body: { code?: string | number, email?: string, mobile: string },
    //     @Param('flag') flag: string
    // ): Promise<void> {
    //     return await this.authService.resendOtp(body,flag);
    // }

    @Post('/verifyOtp/:flag')
    async verifyOtp(
        @Body() body: { otp: string, email: string },
        @Param('flag') flag: string
    ): Promise<any> {
        return await this.authService.verifyOtp(body, flag);
    }
}