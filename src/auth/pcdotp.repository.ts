import { DataSource,  Repository } from "typeorm";
import { Injectable, Logger } from "@nestjs/common";
import {  Pcdotp } from "./pcdotp.entity";
import * as config from 'config';

@Injectable()
export class PcdotpRepository extends Repository<Pcdotp>{
    constructor(private dataSource: DataSource) {
        super(Pcdotp, dataSource.createEntityManager());
      }
    private logger = new Logger('PcdotpRepository'); // for logging


}