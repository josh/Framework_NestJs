import { BadRequestException, Body, Logger, RequestTimeoutException, UnauthorizedException, } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as config from 'config';
import { v4 as uuidv4 } from 'uuid';
const https = require('https');
const fs = require('fs');
import { CryptoService } from './crypto.service'; 
import { AuthCreadentialsDto } from './dto/auth-credentials.dto';
import { TokenDto } from './dto/token.dto'; 
import { JwtPayload } from './jwt-payload.interface';
import { PcduserDto } from 'src/auth/dto/pcd-user.dto';
import { FindManyOptions, In, IsNull, Like, Not, QueryRunner } from 'typeorm';
import { TempSignUpDto } from './dto/temp-sign-up.dto';
import { PcduserRepository } from './pcduser.repository';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { PcdotpDto } from './dto/pcdotp.dto';
import { PcdotpRepository } from './pcdotp.repository';

const { exec } = require("child_process");
import { generateOtp } from 'otp-generator';

const dbConfig = config.get('db');
const oracledb = require('oracledb');
import { Pcduser } from './pcduser.entity';
import { convertDateString } from 'src/pipe/pcd-date.pipe';
import { UserBankDto } from './dto/user-bank.dto';

import { ChangePasswordDto } from './dto/change-password.dto';
import { log } from 'console';
import { version } from 'os';
import { EmailService } from 'src/email/email.service';
export enum Provider {
    GOOGLE = 'google'
}

export class AuthService {
    private logger = new Logger('AuthService'); // for logging

    constructor(
        @InjectRepository(PcduserRepository)
        private readonly pcduserRepository: PcduserRepository,
        private jwtService: JwtService,
        private crypto: CryptoService,
        private readonly pcdotpRepository: PcdotpRepository,
        private mailService: EmailService

    ) {

    }

    async getAll() {
        return await this.pcduserRepository.find();
    }

    async userInfo(authCredentialsDto: AuthCreadentialsDto) {
        const { username, secuLevel } = authCredentialsDto;
        let user = await this.pcduserRepository.findOne({where:{ email: (username.trim()).toLocaleLowerCase() }});
        if (!user) {
            throw new BadRequestException('User Details not found')
        }
        if (user) {
            return { username: user.userName, secuLevel: user.secuLevel, userId: user.userId, mobile: user.mobileNumber, email: user.email }
        }
    }

    async signIn(authCredentialsDto: AuthCreadentialsDto, ip?: string): Promise<TokenDto> {

        const user: PcduserDto = await this.validateUserPassword(authCredentialsDto);

        if (!user) {
            throw new UnauthorizedException('User not Registered')
        }
        return await this.generateTokens(user, user.userName, ip, authCredentialsDto.module, user['changePassword']);
    }


    async validateUserPassword(authCredentialsDto: AuthCreadentialsDto): Promise<Pcduser> {
        const { username, password } = authCredentialsDto;

        let user = await this.pcduserRepository.findOne({where:{ email: (username.trim()).toLocaleLowerCase() }});
        console.log("user=============", user);
        if (!user) {
            throw new UnauthorizedException('User not Registered')
        }

        if (user && await user.validatePassword(password, user)) {
            return user;
        }
        else {
            throw new BadRequestException("Wrong Password");
        }
    }

    async forgotPassword(body: { pass: string, email: string, otp: string }): Promise<any> {
        let iuserData: PcduserDto = await this.pcduserRepository.findOne({ where: { email: body.email } });
        console.log("iuserdata", iuserData);

        const filter: FindManyOptions = { where: { "refCode": body.email, "status": "I" }, order: { code: "DESC" } };
        const newOtp = await this.pcdotpRepository.find(filter);

        if (body.otp != newOtp[0].otp) {
            throw new UnauthorizedException('Unauthorized Access')
        }
        console.log("salt1234");
        const salt = await bcrypt.genSalt();
        iuserData.salt = salt;
        iuserData.pass = await bcrypt.hash(body.pass, salt);
        console.log("salt", iuserData.salt);
        console.log("pass", iuserData.pass);

        try {
            await this.pcduserRepository.save(iuserData);
        }
        catch (err) {
            throw new BadRequestException("Error in Password Reset");
        }
    }


    async sendMail(email: string, custName: string, tenderName: string, bidOpenDate: string, data: string, flag: string) {

        let content: string;
        let subject: string = "Login Credentials";
        if (flag == 'otp') {
            content = `
                <!doctype html>
                <html lang="en-US">
                
                <head>
                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                    <title>OTP</title>
                    <meta name="description" content="New Account Email Template.">
                    <style type="text/css">
                        a:hover {text-decoration: underline !important;}
                    </style>
                </head>
                
                <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                    <!-- 100% body table -->
                    <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                        <tr>
                            <td>
                                <table style="background-color: #f2f3f8; max-width:800px; margin:0 auto;" width="100%" border="0"
                                    align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="height:80px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center;">
                                            <a href="https://www.thrickodithanamscb.in/" title="" target="_blank">
                                            <img width="60" src="https://www.thrickodithanamscb.in/assets/img/emblem3.png" title="logo" alt="logo">
                                          </a>
                                          <h3 style="font-family: "Lucida Console", "Courier New", monospace;">THRICKODITHANAM SCB</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                                style="max-width:670px; background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                <tr>
                                                    <td style="height:40px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0 35px;">
                                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">OTP 
                                                        </h1>
                                                        <p style="font-size:15px; color:#455056; margin:8px 0 0; line-height:24px;">
                                                        Please Register immediately after getting OTP , <br>
                                                        <strong>Your one time password(OTP) for register in tenders is</strong>.</p>
                                                        <span
                                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                        <p
                                                            style="color:#455056; font-size:18px;line-height:20px; margin:0; font-weight: 500;">
                                                           
                                                            <strong
                                                                style="display: block; font-size: 13px; margin: 24px 0 4px 0; font-weight:normal; color:rgba(0,0,0,.64);">OTP</strong>
                                                               
                                                                <h1 style="font-family: "Lucida Console", "Courier New", monospace;"> ${data}</h1>
                                                        </p>
                
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height:40px;">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center;">
                                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy;<strong> <a href="https://www.thrickodithanamscb.in/" >www.thrickodithanamscb.in</a></strong> </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:80px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--/100% body table-->
                </body>
                
                </html> `;
            subject = 'OTP'

        }
        if (flag == 'uDocSub') {
            content = `
            <!doctype html>
            <html lang="en-US">
            
            <head>
                <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                <title>OTP</title>
                <meta name="description" content="New Account Email Template.">
                <style type="text/css">
                    a:hover {text-decoration: underline !important;}
                </style>
            </head>
            
            <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                <!-- 100% body table -->
                <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                    style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                    <tr>
                        <td>
                            <table style="background-color: #f2f3f8; max-width:800px; margin:0 auto;" width="100%" border="0"
                                align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="height:80px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <a href="https://www.thrickodithanamscb.in/" title="" target="_blank">
                                        <img width="60" src="https://www.thrickodithanamscb.in/assets/img/emblem3.png" title="logo" alt="logo">
                                      </a>
                                      <h3 style="font-family: "Lucida Console", "Courier New", monospace;">THRICKODITHANAM SCB</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:20px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                            style="max-width:670px; background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                            <tr>
                                                <td style="height:40px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 35px;">
                                                   
                                                    <p style="font-size:15px; color:#455056; margin:8px 0 0; line-height:24px;">
                                                    Dear ${custName} , <br>
                                                    <strong> We hope this email finds you well. We would like to express our sincere gratitude for your participation in the tender process for <a style="color:red;">${tenderName}</a>. We are pleased to inform you that your tender documents have been successfully submitted.<br><br>
                                                    The bid opening for the tender you have participated in is scheduled for <a style="color:red;">${bidOpenDate}</a>. During this process, all submitted tenders will be carefully reviewed and assessed. We will diligently consider each proposal and evaluate it against the criteria outlined in the tender documentation.<br>
                                                    Should any additional information or documents be required, we will promptly reach out to you via the contact details provided during the submission process.<br><br>
                                                    Should you have any queries or require any clarification regarding the tender process or your submission, please contact our dedicated tender support team. You can reach us via email at tscb178@gmail.com or by calling our helpline at tel:04812441062 or tel:04812443452.<br><br>
                                                  
                                                    Best regards,<br>
                                                    Thrickodithanam Service Co-operative Bank LTD No 178,<br>
                                                    Kunnumpuram, Thrickodithanam,<br> Kottayam, Kerala, 686105.</strong>.</p>
                                                    <span
                                                        style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                   
            
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:40px;">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:20px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy;<strong> <a href="https://www.thrickodithanamscb.in/" >www.thrickodithanamscb.in</a></strong> </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:80px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--/100% body table-->
            </body>
            
            </html> `;
            subject = 'Tender Submission Successful - Thank you for your participation!'
        }

        if (flag == 'fpwd') {
            content = ` <!doctype html>
                <html lang="en-US">
                
                <head>
                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                    <title>OTP</title>
                    <meta name="description" content="New Account Email Template.">
                    <style type="text/css">
                        a:hover {text-decoration: underline !important;}
                    </style>
                </head>
                
                <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                    <!-- 100% body table -->
                    <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                        <tr>
                            <td>
                                <table style="background-color: #f2f3f8; max-width:670px; margin:0 auto;" width="100%" border="0"
                                    align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="height:80px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center;">
                                            <a href="https://www.thrickodithanamscb.in/" title="" target="_blank">
                                            <img width="60" src="https://www.thrickodithanamscb.in/assets/img/emblem3.png" title="logo" alt="logo">
                                          </a>
                                          <h3 style="font-family: "Lucida Console", "Courier New", monospace;">THRICKODITHANAM SCB</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                                style="max-width:670px; background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                <tr>
                                                    <td style="height:40px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0 35px;">
                                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">OTP 
                                                        </h1>
                                                        <p style="font-size:15px; color:#455056; margin:8px 0 0; line-height:24px;">
                                                        Please Register immediately after getting OTP , <br>
                                                        <strong>Your one time password(OTP) for Reset Password is</strong>.</p>
                                                        <span
                                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                        <p
                                                            style="color:#455056; font-size:18px;line-height:20px; margin:0; font-weight: 500;">
                                                           
                                                            <strong
                                                                style="display: block; font-size: 13px; margin: 24px 0 4px 0; font-weight:normal; color:rgba(0,0,0,.64);">OTP</strong>
                                                                <h1 style="font-family: "Lucida Console", "Courier New", monospace;"> ${data}</h1>
                                                        </p>
                
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height:40px;">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center;">
                                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; 
                                            <strong> <a href="https://www.thrickodithanamscb.in/" >www.thrickodithanamscb.in</a></strong> </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:80px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--/100% body table-->
                </body>
                
                </html> `;
            subject = 'OTP'
        }

        if (flag == 'confP') {
            content = `
            <!doctype html>
            <html lang="en-US">
            
            <head>
                <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                <title>OTP</title>
                <meta name="description" content="New Account Email Template.">
                <style type="text/css">
                    a:hover {text-decoration: underline !important;}
                </style>
            </head>
            
            <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                <!-- 100% body table -->
                <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                    style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                    <tr>
                        <td>
                            <table style="background-color: #f2f3f8; max-width:800px; margin:0 auto;" width="100%" border="0"
                                align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="height:80px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <a href="https://www.thrickodithanamscb.in/" title="" target="_blank">
                                        <img width="60" src="https://www.thrickodithanamscb.in/assets/img/emblem3.png" title="logo" alt="logo">
                                      </a>
                                      <h3 style="font-family: "Lucida Console", "Courier New", monospace;">THRICKODITHANAM SCB</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:20px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                            style="max-width:670px; background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                            <tr>
                                                <td style="height:40px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 35px;">
                                                   
                                                    <p style="font-size:15px; color:#455056; margin:8px 0 0; line-height:24px;">
                                                    Dear ${custName} , <br>
                                                    <strong> We hope this email finds you well. We are writing to inform you that your payment for the tender documents has been successfully processed and accepted by our bank. You can now proceed to download the tender documents as per your requirement. <br><br>
                                                    Once again, we appreciate your prompt payment and cooperation. We look forward to receiving your tender submission.<br>
                                                    Thank you for your participation.<br><br>
                                                    Should you have any queries or require any clarification regarding the tender process or your submission, please contact our dedicated tender support team. You can reach us via email at tscb178@gmail.com or by calling our helpline at tel:04812441062 or tel:04812443452.<br><br>
                                                  
                                                    Best regards,<br>
                                                    Thrickodithanam Service Co-operative Bank LTD No 178,<br>
                                                    Kunnumpuram, Thrickodithanam,<br> Kottayam, Kerala, 686105.</strong>.</p>
                                                    <span
                                                        style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                   
            
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:40px;">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:20px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy;<strong> <a href="https://www.thrickodithanamscb.in/" >www.thrickodithanamscb.in</a></strong> </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:80px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--/100% body table-->
            </body>
            
            </html> `;
            subject = 'Payment Confirmation - Tender Document Download Available'
        }

        await this.mailService.sendMail(email, subject, content, content);

    }



    async sendOtp(body: { email: string, custName: string, tenderName?: string, bidOpenDate?: string }, flag: string): Promise<void> {

        if (body.email) {
            if (flag == 'F' || flag == 'R' || flag == 'UD' || flag == 'CP') {

                let isUserExist = await this.pcduserRepository.findOne({ where: { email: body.email } });

                console.log("isUserExist", isUserExist);  

                if (!isUserExist && flag == "F") {
                    throw new BadRequestException("User not found");
                }
                if (flag == 'R' && isUserExist) {
                    throw new BadRequestException("Already registered with this email Id.");
                } else {
                    let newOtp = await this.generateOtp(body.email, flag);
                    if (body.email && flag == 'R') {
                        await this.sendMail(body.email, body.custName, body.tenderName, body.bidOpenDate, newOtp, 'otp');
                    }
                    if (body.email && flag == 'F') {
                        await this.sendMail(body.email, body.custName, body.tenderName, body.bidOpenDate, newOtp, 'fpwd');
                    }
                    if (body.email && flag == 'UD') {
                        await this.sendMail(body.email, body.custName, body.tenderName, body.bidOpenDate, newOtp, 'uDocSub');
                    }
                    if (body.email && flag == 'CP') {
                        await this.sendMail(body.email, body.custName, body.tenderName, body.bidOpenDate, newOtp, 'confP');
                    }

                }

            } err => {
                console.log(err, "hello12345");

            };


        }
    }

    async tempSignUp(pcdUserDto: PcduserDto): Promise<any> {
        const filter: FindManyOptions = { where: { "refCode": pcdUserDto.email, "status": "I" }, order: { code: "DESC" } };
        const newOtp = await this.pcdotpRepository.find(filter);
        const existingUser = await this.pcduserRepository.findOne({ where: { email: pcdUserDto.email } });
        if (existingUser) {
            throw new BadRequestException('User Already Exists');
        }
        if (pcdUserDto.otp != newOtp[0].otp) {
            throw new UnauthorizedException('Unauthorized Access');
        }

        //Creating Random User_Id for every user registered
        const lastUser: PcduserDto = await this.pcduserRepository.findOne({
            where: {},
            order: { userId: 'DESC' },
        });
        let maxCode = 10000;
        if (lastUser && lastUser.userId) {
            const lastUserId = parseInt(lastUser.userId);
            maxCode = Math.max(maxCode, lastUserId);
        }
        const newUserId = String(maxCode + 1).padStart(5, '0');
        /////////////////////////////////////////////////////////////

        pcdUserDto.userId = newUserId;
        const salt = await bcrypt.genSalt();
        pcdUserDto.salt = salt;
        pcdUserDto.pass = await bcrypt.hash(pcdUserDto.pass, salt);
        pcdUserDto.secuLevel = 'U';
        delete pcdUserDto['otp'];
        await this.pcduserRepository.save(pcdUserDto);
        this.logger.verbose("User created with email:", pcdUserDto.email);
        return { data: "Successfully Registered" };
    }


    async verifyOtp(body: { otp: string, email: string }, flag: string): Promise<any> {

        console.log('body===', body, flag);

        const filter: FindManyOptions = { where: { "refCode": body.email, "status": "I" }, order: { code: "DESC" } };


        const newOtp = await this.pcdotpRepository.find(filter);
        if (!newOtp)
            throw new BadRequestException("OTP Error. Resend OTP");
        let currentDateTime = new Date().valueOf();
        let otpTime = newOtp[0].createdDate.valueOf();
        let diffMs = (currentDateTime - otpTime);
        let otpExpiry = 600000; //otp expire in 10 minutes

        let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);

        if (diffMins > config.get('otp').expiresIn)
            throw new RequestTimeoutException('*OTP Expired');


        if (diffMs > otpExpiry)
            throw new RequestTimeoutException('*OTP Expired');

        if (newOtp[0].otp != body.otp)
            throw new UnauthorizedException("*Invalid OTP");
        if (newOtp[0].otp == body.otp) {
            return { data: 'success' };
        }

    }
    // async resendOtp(body: { code?: string | number, email?: string, mobile: string }, flag: string): Promise<void> {
    //     // const newOtp = await this.generateOtp(body.code.toString(), 'RR');

    //     // await this.smsService.sendSms(body.mobile, ` Your one time password(OTP) for Accurate Application Registration is,${newOtp}`);
    //     const isUserExist = await this.pcduserRepository.findOne({ where: { mobileNumber: body.mobile } });
    //     const newOtp = await this.generateOtp(isUserExist.userId.toString(), flag);

    //     if (body.mobile)
    //         await this.smsService.sendSms(body.mobile, newOtp);

    // }


    async generateOtp(refCode: string, flag: string): Promise<string> {

        var otpGenerator = require('otp-generator')

        const newOtp = await otpGenerator.generate(6, { upperCase: false, specialChars: false, alphabets: false });
        console.log("===newOtp====", newOtp);

        let usedFor: string = ''; 
 

        switch (flag) {
            case "R":
                usedFor = "REGISTRATION";
                break;
            case "RR":
                usedFor = "REGISTRATION RESEND"
                break;
            case "F":
                usedFor = "FORGOT PASSWORD";
                break;
            case "FR":
                usedFor = "FORGOT PASSWORD RESEND";
                break;

        }
        try {

            let igotp: PcdotpDto = new PcdotpDto();
            igotp.otp = newOtp;
            igotp.createdBy = 'SYSTEM';
            igotp.createdDate = new Date();
            igotp.modifiedBy = 'SYSTEM';
            igotp.modifiedDate = new Date();
            igotp.refCode = refCode;
            igotp.usedFor = usedFor;
            igotp.status = 'I';
            await this.pcdotpRepository.save(igotp);
            return newOtp;
        }
        catch (err) {
            console.log("====otperr===", err);
        }

    }


    /* The below method is used for generating the access token and refresh token when login */
    async generateTokens(user: PcduserDto, username: string, ip: string, module: string, changePassword?: string): Promise<TokenDto> {

        const sessionId = (Math.floor((Math.random() * 100000000) + 1)).toString();
        const payload: JwtPayload = { userId: user.userId.trim(), userName: user.userName.trim(), emailId: user.email, module: module, sessionId: sessionId };
        const accessToken = await this.jwtService.sign(payload, { expiresIn: config.get('jwt').expiresIn });

        const refreshToken = uuidv4();
        await this.pcduserRepository.updateRefreshToken(user.userId, refreshToken);


        this.logger.debug(`generated JWT Token with Payload ${JSON.stringify(payload)}`);
        const result: TokenDto = { accessToken: this.crypto.set(config.get('crypto').accessToken, accessToken), refreshToken: this.crypto.set(config.get('crypto').accessToken, refreshToken), emailId: user.email, module: module, changePassword: changePassword };

        return result;
    }

    private async hashPassword(password: string, salt: string): Promise<string> {
        return await bcrypt.hash(password, salt);
    }


}