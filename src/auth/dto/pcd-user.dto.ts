export class PcduserDto {

    userId: string;

    userName: string;

    groupOrUser: string;

    pass: string | null;

    salt: string | null;

    secuLevel: string | null;

    pwdExpDays: number | null;

    email: string;

    mobileNumber: string;

    userImagePath: string;

    refreshtoken: string;

    memberNo: string;

    custId: string;

    status: string;

    bankId: string;
    
    tPass: string | null;

    tSalt: string | null;

    deviceId:string;

    otp?:string;
}