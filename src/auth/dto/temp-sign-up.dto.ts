import { IsString, MinLength, MaxLength, Matches, IsNotEmpty, IsNumber, IsIn } from "class-validator";

export class TempSignUpDto {

    // @IsString()
    // @MinLength(0)
    // @MaxLength(12)
    // @IsNotEmpty()
    email: string;



    @IsString()
    @MinLength(2)
    @MaxLength(20)
    username: string;


    @IsString()
    @MinLength(4)
    @MaxLength(20)
    password: string;
    
    tpassword?: string;

    mobile?:string;
   
}