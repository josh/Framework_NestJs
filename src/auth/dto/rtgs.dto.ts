import { IsNotEmpty } from "class-validator";

export class RtgsDto {

    rtgsId: string;

    brCode: string;

    dayDt: string;

    code: string;

    @IsNotEmpty()
    accNo: string;

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    toAccNo: string;

    @IsNotEmpty()
    toParti: string;

    toEmail: null;

    toMobile: null;

    @IsNotEmpty()
    toBank: string;

    @IsNotEmpty()
    toBranch: string;

    @IsNotEmpty()
    ifscCode: string;

    cashAdj: string;

    payMode: string;

    remarks: string;

    uploadStatus: string;

    credit: null;

    @IsNotEmpty()
    debit: number;

    slipNo: number;

    clerk: string;

    cashier: string;

    manager: string;

    clerkId: number;

    mgrId: number;

    cashierId: null;

    rejectReason: null;

    entryTime: string;

    appScr: string;

    @IsNotEmpty() 
    commAmt: number;

    chNo: null;

    chDate: null;

    dayDate: any;

    @IsNotEmpty() 
    depType: any;

    dayBeginDt: any;
}
