export class TokenDto {

    accessToken: string;

    refreshToken: string;

    emailId: string;

    module: string;

    changePassword?:string

    secuLevel?:string
}