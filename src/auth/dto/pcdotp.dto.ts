import { PcdBaseDto } from "src/framework/custom/pcd-dto.custom";

export class PcdotpDto extends PcdBaseDto{

    code: number;

    otp: string;

    refCode: string;

    usedFor: string;

    status: string;

}