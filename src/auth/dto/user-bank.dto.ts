
export class UserBankDto {
    custId:string;

    custName:string;

    mobile:string;

    mPin:string;

    tPin:string;

    email:string;

    bankId:string;
}
