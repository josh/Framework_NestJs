import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import {Pcduser} from "src/auth/pcduser.entity";

export const GetUser = createParamDecorator(
    (data: unknown, ctx: ExecutionContext):Pcduser => {
      const request = ctx.switchToHttp().getRequest();
      return request.user;
    },
  );