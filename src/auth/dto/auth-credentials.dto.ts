import { IsNotEmpty, IsString, MaxLength, maxLength, MinLength } from "class-validator";

export class AuthCreadentialsDto {


    @IsString()
    @MinLength(2)
    @MaxLength(100)
    username: string;
    
    password: string;

    module?: string;

    custId: string;

    email: string;

    secuLevel: any;

}