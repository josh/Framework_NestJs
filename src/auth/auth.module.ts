import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

import * as config from 'config';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CryptoService } from './crypto.service';
import { JwtStrategy } from './jwt.stategy';
import { PcduserRepository } from './pcduser.repository';
import { PcdotpRepository } from './pcdotp.repository';
import { EmailService } from 'src/email/email.service';
import { EmailModule } from 'src/email/email.module';
import { Pcduser } from './pcduser.entity';
import { Pcdotp } from './pcdotp.entity';


const jwtConfig = config.get('jwt');

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.JWT_SECRET || jwtConfig.secret,
      signOptions: {
        expiresIn: jwtConfig.expiresIn, 
      }
    }),
    TypeOrmModule.forFeature([
      Pcduser,
      Pcdotp,
   
    ]),EmailModule,
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    CryptoService,
     JwtStrategy,
     PcdotpRepository,
     PcduserRepository
     
     
   
  ],
  exports: [
    JwtStrategy,
    JwtModule,
    PassportModule,
    CryptoService
  ],
})
export class AuthModule { }
